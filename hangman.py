# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random, string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist


def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()



def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise

    '''
    count = 0
    for i in secret_word:
        if i in letters_guessed:
            count += 1
    if count == len(secret_word):
        return True
    else:
        return False


def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    guess_progress_lt = []
    for i in secret_word:                    #goes threw letters in the str secret_word
        if i in letters_guessed:
            guess_progress_lt.append(i)
        else:
            guess_progress_lt.append("_ ")
    string_guess = "".join(guess_progress_lt)
    return string_guess



def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    alpha = list(string.ascii_lowercase)
    for i in alpha:
        if i in letters_guessed:
            alpha.remove(i)
    alpha_str = "".join(alpha)
    return alpha_str


def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    print("------------------------------------------")
    print("Hey there! There are 6 guesses in this game for a word of length:", len(secret_word))
    letters_guessed = []
    lives = 3    # (warnings)
    guesses = 6
    vowelLower = ["a", "e", "i", "o", "u"]
    vowelUpper = ["A", "E", "I", "O", "U"]
    consonantLower = list(string.ascii_lowercase)
    consonantUpper = list(string.ascii_uppercase)
    for i in vowelLower:
        consonantLower.remove(i)
    for i in vowelUpper:
        consonantUpper.remove(i)


    while is_word_guessed(secret_word, letters_guessed) == False:
        print(" ** This is where you're at so far: ", get_guessed_word(secret_word, letters_guessed))
        print(" ** You have these letters left to guess: ", get_available_letters(letters_guessed))
        print(" ** Remaining guesses: ", guesses)
        print("--------------------------------------")

        guess = input("Input a letter please <3: " )
        letters_guessed.append(str(guess))
        if letters_guessed[-1] not in (list(string.ascii_lowercase) or list(string.ascii_uppercase)) or (letters_guessed[-1] in letters_guessed[:-1]):
            lives -= 1
            print("***** WARNING ***** : That is against the rules! You have: ", lives," warnings left!")
            if lives < 1:
                guesses -= 1
                print("You're out of warnings so you lose a guess sorry!")
            else:
                print("You have ", lives, "warnings left!")
        if letters_guessed[-1] not in list(secret_word):
            if letters_guessed[-1] in vowelLower or vowelUpper:
                guesses -= 2
            elif letters_guessed[-1] in consonantLower or consonantUpper:
                guesses -= 1
        if guesses == 0:
            print("GAME OVER.")
            break
        if is_word_guessed(secret_word, letters_guessed) == True:
            print("Well played you won !! The word was ", secret_word)



# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test
#(hint: you might want to pick your own
# secret_word while you're doing your own testing)


# -----------------------------------



def match_with_gaps(my_word, other_word):

    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    word_no_white = my_word.replace(" ", "")

    def multiLetterMistake(word_no_white, other_word):    #checks for cases like a_ple that cannot be apple
        for i in range(len(other_word)):
            if word_no_white[i] != "_" and word_no_white.count(word_no_white[i]) != other_word.count(other_word[i]):
                return True

    if word_no_white == other_word:
        return True

    for i in range(len(other_word)):
        if word_no_white[i] == "_" or word_no_white[i] == other_word[i]:
            could_be_same = True
        else:
            return False

    if (could_be_same is True) and (multiLetterMistake(word_no_white, other_word) != True) and (other_word in wordlist):
        return True
    else:
        return False




def show_possible_matches(my_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    word_possible_list = []
    for i in wordlist:
        if match_with_gaps(my_word, i) is True:
            word_possible_list.append(i)

    word_possible_str = ", ".join(word_possible_list)
    return word_possible_str




def hangman_with_hints(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses
    
    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter
      
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
      
    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word. 
    
    Follows the other limitations detailed in the problem write-up.
    '''
    print("------------------------------------------")
    print("Hey there! There are 6 guesses in this game for a word of length:", len(secret_word))
    letters_guessed = []
    lives = 3    # (warnings)
    guesses = 6
    vowelLower = ["a", "e", "i", "o", "u"]
    vowelUpper = ["A", "E", "I", "O", "U"]
    consonantLower = list(string.ascii_lowercase)
    consonantUpper = list(string.ascii_uppercase)
    for i in vowelLower:
        consonantLower.remove(i)
    for i in vowelUpper:
        consonantUpper.remove(i)


    while is_word_guessed(secret_word, letters_guessed) == False:
        print(" ** This is where you're at so far: ", get_guessed_word(secret_word, letters_guessed))
        print(" ** You have these letters left to guess: ", get_available_letters(letters_guessed))
        print(" ** Remaining guesses: ", guesses)
        print("--------------------------------------")
        letters_guessed.append(input("Input a letter please <3: ", ))
        if letters_guessed[-1] == "*":
            print(show_possible_matches(get_guessed_word(secret_word, letters_guessed)))
        else:
            if letters_guessed[-1] not in (list(string.ascii_lowercase) or list(string.ascii_uppercase)) or (letters_guessed[-1] in letters_guessed[:-1]):
                lives -= 1
                print("***** WARNING ***** : That is against the rules! You have: ", lives," warnings left!")
                if lives < 1:
                    guesses -= 1
                    print("You're out of warnings so you lose a guess sorry!")
                else:
                    print("You have ", lives, "warnings left!")
            if letters_guessed[-1] not in list(secret_word):
                if letters_guessed[-1] in vowelLower or vowelUpper:
                    guesses -= 2
                elif letters_guessed[-1] in consonantLower or consonantUpper:
                    guesses -= 1
            if guesses == 0:
                print("GAME OVER.")
                break
            if is_word_guessed(secret_word, letters_guessed) == True:
                print("Well played you won !! The word was ", secret_word)




# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


if __name__ == "__main__":
    #pass

    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.
    
    #secret_word = choose_word(wordlist)
    #hangman(secret_word)

###############
    
    # To test part 3 re-comment out the above lines and 
    # uncomment the following two lines. 
    
    secret_word = choose_word(wordlist)
    hangman_with_hints(secret_word)
